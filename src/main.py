import sys
import logging

from glob import glob
from crackers import crackers


logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


BASE_CAPS_PATH = '/resources/caps'


def main():
    args = sys.argv
    cracker_name = args[1]
    cracker = crackers.get(cracker_name)
    if not cracker:
        logger.error(f'invalid cracker => {cracker_name}')
        logger.error(f'valid crackers are => {list(crackers.keys())}')
        return

    hash_file = args[2]
    hash_file_paths = glob(f'{BASE_CAPS_PATH}/**/{hash_file}')
    if not hash_file_paths:
        logger.error(f'hash_file {hash_file} not found!')
        return

    hash_file_path = hash_file_paths[0]
    logger.info(f'hash_file found at => {hash_file_path}')
    dict_name = args[3]
    cracker(hash_file_path, dict_name)


if __name__ == '__main__':
    main()
