import logging

from dictionaries import dicts
from utils.commands import cap2hccapx, wpa_crack


logger = logging.getLogger(__name__)


def wpa_cracker(hash_file_path: str, dict_name: str):
    hccapx_file_path = cap2hccapx(hash_file_path)
    dicts_ = dicts.get(dict_name)
    if not dicts_:
        logger.error(f'invalid dict => {dict_name}')
        logger.error(f'valid dicts are => {list(dicts.keys())}')
        return

    for dictionary in dicts_:
        msg = f'\n{">"*150}\ntrying with dictionary => {dictionary}\n{">"*150}'
        logger.info(msg)
        password = wpa_crack(hccapx_file_path, dictionary)
        if password:
            msg = f'\n{"@"*50}\npassword found => {password}\n{"@"*50}'
            logger.info(msg)
            break


crackers = {
    'wpa': wpa_cracker
}
