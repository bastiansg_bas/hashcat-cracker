import logging
import threading
import subprocess


logger = logging.getLogger(__name__)


def stdout_printer(p):
    for line in p.stdout:
        logger.info(line.rstrip())


def execute(command: str):
    try:
        logger.info(f'running command => {command}')
        command_array = command.split()
        p = subprocess.Popen(
            command_array,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True
        )

        t = threading.Thread(target=stdout_printer, args=(p, ))
        t.start()
        p.stdin.close()
        t.join()

    except subprocess.CalledProcessError as e:
        logger.error(e)
