import os
import logging

from typing import Optional
from .popen_execute import execute


logger = logging.getLogger(__name__)


def cap2hccapx(cap_file_path: str):
    out_file = cap_file_path.split('/')[-1].replace('.cap', '.hccapx')
    out_file_path = f'/tmp/{out_file}'
    command = f'cap2hccapx {cap_file_path} {out_file_path}'
    execute(command)
    return out_file_path


def wpa_crack(hccapx_file_path: str, dictionary: str) -> Optional[str]:
    out_file = '/tmp/hcc_out.txt'
    command = f'hashcat -m 2500 {hccapx_file_path} {dictionary} -o {out_file}'

    execute(command)
    if not os.path.exists(out_file):
        return

    with open(out_file, 'r') as f:
        recovered = f.read().splitlines()[0]
        password = recovered.split(':')[-1]
        return password
