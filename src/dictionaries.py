FIBERTEL_BASE_PATH = '/resources/dictionaries/fibertel'


dicts = {
    'fibertel': [
        f'{FIBERTEL_BASE_PATH}/004-dni.txt',
        f'{FIBERTEL_BASE_PATH}/014-dni.txt',
        f'{FIBERTEL_BASE_PATH}/004+7.txt',
        f'{FIBERTEL_BASE_PATH}/014+7.txt'
    ]
}
