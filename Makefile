HCC_IMAGE = hashcat-cracker
VERSION = 1.0.0

env-setup:
	pipenv install --skip-lock

hcc-build:
	docker build -t ${HCC_IMAGE}:$(VERSION) \
		-f ./build/hashcat-cracker/Dockerfile .
	docker tag ${HCC_IMAGE}:$(VERSION) ${HCC_IMAGE}:latest

hcc-run: check-parameters
	docker run -it --rm \
		--privileged --network host \
		--gpus all --shm-size 8G \
		-v $(PWD)/src:/src \
		-v $(PWD)/resources:/resources \
		-e CRACKER=${CRACKER} \
		-e HASH_FILE=${HASH_FILE} \
		-e DICT_NAME=${DICT_NAME} \
		${HCC_IMAGE}:$(VERSION)

check-parameters:
ifndef CRACKER
	$(error parameter CRACKER is required)
endif

ifndef HASH_FILE
	$(error parameter HASH_FILE is required)
endif

ifndef DICT_NAME
	$(error parameter DICT_NAME is required)
endif
