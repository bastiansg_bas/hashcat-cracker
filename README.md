# Hashcat Cracker

_Docker hashcat cracker with nvidia GPU support_
<br/>
<br/>

Setup
----
<br/>

```bash
$ git clone https://gitlab.com/bastiansg_bas/hashcat-cracker
```

This repo requires Docker 20.10 or above

You also need the latest version of [nvidia-container-toolkit](https://github.com/NVIDIA/nvidia-docker)

You can install nvidia-container-toolkit as follows:

```bash
$ distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
$ curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
$ curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

$ sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
$ sudo systemctl restart docker
```
<br/>

Usage
-----

You can do _make build_ and _make run_ with the required envs **CRACKER**, **HASH_FILE** and **DICT_NAME**

### Example

```bash
$ make run CRACKER=wpa HASH_FILE=FibertelWiFi768-01.cap DICT_NAME=fibertel
```
